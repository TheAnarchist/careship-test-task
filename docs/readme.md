# Documentation

## Endpoints

* [Withdraw from balance](cash-machine.md) : `POST /v1/cash-machine/withdraw/`

## Errors

When errors occurs the server responds with appropriate HTTP-code and payload. 
All payload have the same format.

For example:

```json
{
    "code": 400,
    "message": "Missing required parameter 'amounts'"
}
```

[Back](../readme.md)
