# Cash Machine

## The Problem
Develop a solution that simulate the delivery of notes when a client does a withdraw in a cash machine.
The basic requirements are the follow:
- Always deliver the lowest number of possible notes;
- It’s possible to get the amount requested with available notes;
- The client balance is infinite;
- Amount of notes is infinite;
- Available notes $ 100,00; $ 50,00; $ 20,00 e $ 10,00

## Examples:
Entry   | Result
------- | -------
30.00   | [20.00, 10.00]
80.00   | [50.00, 20.00, 10.00]
125.00  | throw NoteUnavailableException
-130.00 | throw InvalidArgumentException
NULL    | [Empty Set]

[Back](readme.md)