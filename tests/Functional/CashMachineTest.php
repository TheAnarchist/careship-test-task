<?php

namespace Tests\Functional;

use App\Common\CustomErrorHandler;
use App\Exception\WrongRequestException;
use App\Exception\InvalidArgumentException;
use App\Exception\NoteUnavailableException;

class CashMachineTest extends BaseTestCase
{
    const ENDPOINT = '/v1/cash-machine/withdraw';
    const METHOD = 'POST';

    public function testMissingParameter()
    {
        $response = $this->runApp(self::METHOD, self::ENDPOINT, []);
        $actualResponseBody = (string) $response->getBody();
        $expectedResponse = json_encode([
            "error_code" => WrongRequestException::CODE_MISSING_REQUIRED_PARAMETER,
            "error_message" => sprintf(WrongRequestException::MESSAGE_MISSING_REQUIRED_PARAMETER, 'amount'),
        ]);

        $this->assertEquals(CustomErrorHandler::STATUS_BAD_REQUEST, $response->getStatusCode());
        $this->assertEquals($expectedResponse, $actualResponseBody);
    }

    public function testWrongFormat()
    {
        $response = $this->runApp(self::METHOD, self::ENDPOINT, ["amount" => "100"]);
        $actualResponseBody = (string) $response->getBody();
        $expectedResponse = json_encode([
            "error_code" => WrongRequestException::CODE_INCORRECT_FORMAT,
            "error_message" => sprintf(WrongRequestException::MESSAGE_INCORRECT_FORMAT, 'amount'),
        ]);

        $this->assertEquals(CustomErrorHandler::STATUS_BAD_REQUEST, $response->getStatusCode());
        $this->assertEquals($expectedResponse, $actualResponseBody);
    }

    public function testNegativeAmount()
    {
        $response = $this->runApp(self::METHOD, self::ENDPOINT, ["amount" => -100]);
        $actualResponseBody = (string) $response->getBody();
        $expectedResponse = json_encode([
            "error_code" => InvalidArgumentException::NEGATIVE_NUMBER_CODE,
            "error_message" => "Amount should be positive",
        ]);

        $this->assertEquals(CustomErrorHandler::STATUS_BAD_REQUEST, $response->getStatusCode());
        $this->assertEquals($expectedResponse, $actualResponseBody);
    }

    public function testUnavailableNote()
    {
        $response = $this->runApp(self::METHOD, self::ENDPOINT, ["amount" => 95]);
        $actualResponseBody = (string) $response->getBody();
        $expectedResponse = json_encode([
            "error_code" => NoteUnavailableException::UNAVAILABLE_NOTE_CODE,
            "error_message" => "Requested notes unavailable",
        ]);

        $this->assertEquals(CustomErrorHandler::STATUS_BAD_REQUEST, $response->getStatusCode());
        $this->assertEquals($expectedResponse, $actualResponseBody);
    }

    public function testSuccess()
    {
        $response = $this->runApp(self::METHOD, self::ENDPOINT, ["amount" => 10]);
        $actualResponseBody = (string) $response->getBody();
        $expectedResponse = json_encode([
            "notes" => [10],
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($expectedResponse, $actualResponseBody);
    }
}