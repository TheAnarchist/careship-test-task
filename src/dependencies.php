<?php

use Psr\Container\ContainerInterface;
use App\CashMachine\Controller as WithdrawController;
use App\CashMachine\WithdrawService;
use App\Common\CustomErrorHandler;
use App\Common\NotFoundHandler;

// DIC configuration
$container = $app->getContainer();

/**
 * @param ContainerInterface $c
 * @return \Monolog\Logger
 */
$container['logger'] = function ( $c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

/**
 * Override the default Error Handler
 *
 * @param ContainerInterface $c
 * @return CustomErrorHandler
 */
$container['errorHandler'] = function ($c) {
    return new CustomErrorHandler($c->get('logger'));
};

/**
 * Override the default Not Found Handler
 *
 * @param ContainerInterface $c
 * @return NotFoundHandler
 */
$container['notFoundHandler'] = function ($c) {
    return new NotFoundHandler($c->get('logger'));
};

/**
 * Withdraw service
 * @return WithdrawService
 */
$container[WithdrawService::class] = function () {
    return new WithdrawService;
};

/**
 * Cash Machine controller
 *
 * @param ContainerInterface $container
 * @return WithdrawController
 */
$container[WithdrawController::class] = function (ContainerInterface $container) {
    return new WithdrawController($container);
};
