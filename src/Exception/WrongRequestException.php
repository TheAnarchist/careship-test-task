<?php

namespace App\Exception;

class WrongRequestException extends \Exception
{
    const CODE_MISSING_REQUIRED_PARAMETER = 1001;
    const MESSAGE_MISSING_REQUIRED_PARAMETER = "Missing required parameter '%s'";

    const CODE_INCORRECT_FORMAT = 1002;
    const MESSAGE_INCORRECT_FORMAT = "Incorrect format of parameter '%s'";
}