<?php

namespace App\Exception;

class NoteUnavailableException extends \Exception
{
    const UNAVAILABLE_NOTE_CODE = 3001;
}