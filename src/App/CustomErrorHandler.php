<?php

namespace App\Common;

use Monolog\Logger;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Exception\WrongRequestException;
use App\Exception\InvalidArgumentException;
use App\Exception\NoteUnavailableException;

class CustomErrorHandler
{
    const STATUS_BAD_REQUEST = 400;
    const STATUS_INTERNAL_ERROR = 500;

    /** @var Logger */
    protected $logger;

    /** @var array */
    protected $badRequestExceptions = [
        WrongRequestException::class,
        InvalidArgumentException::class,
        NoteUnavailableException::class,
    ];

    /**
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param  Request $request
     * @param  Response $response
     * @param  \Exception $exception
     *
     * @return Response
     */
    public function __invoke($request, $response, $exception)
    {
        $responseBody = [
            "error_code" => $exception->getCode(),
            "error_message" => $exception->getMessage(),
        ];

        return $response->withJson($responseBody, $this->getStatus($exception));
    }

    /**
     * @param $exception
     * @return int
     */
    protected function getStatus($exception)
    {
        $exceptionClass = get_class($exception);
        if (in_array($exceptionClass, $this->badRequestExceptions)) {
            return self::STATUS_BAD_REQUEST;
        }

        $errorMessage = $this->getErrorMessage($exception);
        $this->logger->error($errorMessage);

        return self::STATUS_INTERNAL_ERROR;
    }

    /**
     * @param \Exception $exception
     * @return string
     */
    protected function getErrorMessage($exception) {
        return sprintf(
            "%s - Exception %s with trace %s",
            date('Y-m-d H:i:s'),
            get_class($exception),
            $exception->getTraceAsString()
        );
    }
}