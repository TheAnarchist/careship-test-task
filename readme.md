# Careship Test Task

[Task text](task_text.md)

## Install the Application

- `git clone <repo> <folder-name>`
- `cd <folder-name>`
- `composer install`

## Run app and tests

- `composer start` to run application
- `composer test` to run tests
- Docker way to run application: `docker-composer up -d`

## Documentation

[documentation](docs/readme.md)